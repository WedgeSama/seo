<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model;

/**
 * Class Rating
 *
 * @author Benjamin Georgeault
 */
final class Rating implements MetaTagInterface
{
    private RatingEnum $rating = RatingEnum::GENERAL;

    public static function create(RatingEnum $rating = RatingEnum::GENERAL): Rating
    {
        return (new Rating())
            ->setRating($rating)
        ;
    }

    public function getName(): string
    {
        return 'rating';
    }

    public function getContent(): ?string
    {
        return $this->rating->value;
    }

    public function setRating(RatingEnum $rating): Rating
    {
        $this->rating = $rating;
        return $this;
    }
}
