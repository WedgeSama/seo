<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model;

/**
 * Class Custom
 *
 * @author Benjamin Georgeault
 */
final class Custom implements MetaTagInterface
{
    private string $name = 'custom';

    private string $content = '';

    public static function create(string $name, string $content): Custom
    {
        return (new Custom())
            ->setName($name)
            ->setContent($content)
        ;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setName(string $name): Custom
    {
        $this->name = $name;
        return $this;
    }

    public function setContent(string $content): Custom
    {
        $this->content = $content;
        return $this;
    }
}
