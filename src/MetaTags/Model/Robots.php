<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model;

use WS\Library\Seo\MetaTags\Model\RobotRules\RuleInterface;
use WS\Library\Seo\MetaTags\Model\RobotRules\TypeEnum;

/**
 * Class Robots
 *
 * @author Benjamin Georgeault
 */
final class Robots implements MetaTagInterface
{
    private string $name = 'robots';

    /** @var null|iterable<RuleInterface> */
    private ?iterable $robotRules = null;

    /**
     * @param iterable<RuleInterface> $robotRules
     */
    public static function create(iterable $robotRules, string $name = 'robots'): Robots
    {
        return (new self())
            ->setRobotRules($robotRules)
            ->setName($name)
        ;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Robots
    {
        $this->name = $name;
        return $this;
    }

    public function getContent(): ?string
    {
        if (null === $this->robotRules) {
            return null;
        }

        $rules = [];
        foreach ($this->robotRules as $rule) {
            if (null === $rule->getValue()) {
                continue;
            }

            $rules[] = match ($rule->getType()) {
                TypeEnum::BOOL => $rule->getName(),
                TypeEnum::STRING => sprintf(
                    '%s:%s',
                    $rule->getName(),
                    str_replace(' ', '', $rule->getValue()),
                ),
                TypeEnum::INT => sprintf('%s:%d', $rule->getName(), $rule->getValue()),
                TypeEnum::DATETIME => sprintf('%s:%s', $rule->getName(), $rule->getValue()->format('c')),
            };
        }

        return implode(', ', $rules);
    }

    /**
     * @return null|iterable<RuleInterface>
     */
    public function getRobotRules(): ?iterable
    {
        return $this->robotRules;
    }

    public function setRobotRules(?iterable $robotRules): Robots
    {
        $this->robotRules = $robotRules;
        return $this;
    }
}
