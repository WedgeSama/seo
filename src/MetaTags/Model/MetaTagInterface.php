<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model;

/**
 * MetaTagInterface is the base interface for all meta tags that you can put in <head> for SEO purposes.
 *
 * @author Benjamin Georgeault
 * @see https://developers.google.com/search/docs/crawling-indexing/special-tags
 */
interface MetaTagInterface
{
    public function getName(): string;

    public function getContent(): ?string;
}
