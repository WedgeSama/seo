<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class NoImageIndex
 *
 * @author Benjamin Georgeault
 */
final class NoImageIndex implements RuleInterface
{
    public static function create(): NoImageIndex
    {
        return new self();
    }

    public function getName(): string
    {
        return 'noimageindex';
    }

    public function getType(): TypeEnum
    {
        return TypeEnum::BOOL;
    }

    public function getValue(): true
    {
        return true;
    }
}
