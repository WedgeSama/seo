<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class NoSnippet
 *
 * @author Benjamin Georgeault
 */
final class NoSnippet implements RuleInterface
{
    public static function create(): NoSnippet
    {
        return new self();
    }

    public function getName(): string
    {
        return 'nosnippet';
    }

    public function getType(): TypeEnum
    {
        return TypeEnum::BOOL;
    }

    public function getValue(): true
    {
        return true;
    }
}
