<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

enum TypeEnum: string
{
    case BOOL = 'bool';

    case INT = 'int';

    case STRING = 'string';

    case DATETIME = 'datetime';
}
