<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class NoTranslate
 *
 * @author Benjamin Georgeault
 */
final class NoTranslate implements RuleInterface
{
    public static function create(): NoTranslate
    {
        return new self();
    }

    public function getName(): string
    {
        return 'notranslate';
    }

    public function getType(): TypeEnum
    {
        return TypeEnum::BOOL;
    }

    public function getValue(): true
    {
        return true;
    }
}
