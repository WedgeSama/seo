<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class UnavailableAfter
 *
 * @author Benjamin Georgeault
 */
final class UnavailableAfter implements RuleInterface
{
    private ?\DateTimeInterface $dateTime = null;

    public static function create(\DateTimeInterface $dateTime): UnavailableAfter
    {
        return (new self())
            ->setDateTime($dateTime)
        ;
    }

    public function getName(): string
    {
        return 'unavailable_after';
    }

    public function getType(): TypeEnum
    {
        return TypeEnum::DATETIME;
    }

    public function getValue(): ?\DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(?\DateTimeInterface $dateTime): UnavailableAfter
    {
        $this->dateTime = $dateTime;
        return $this;
    }
}
