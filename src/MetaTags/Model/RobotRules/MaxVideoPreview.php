<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class MaxVideoPreview
 *
 * @author Benjamin Georgeault
 */
final class MaxVideoPreview implements RuleInterface
{
    private int $maxVideoPreview = -1;

    public static function create(int $maxVideoPreview): MaxVideoPreview
    {
        return (new self())
            ->setMaxVideoPreview($maxVideoPreview)
        ;
    }

    public function getName(): string
    {
        return 'max-video-preview';
    }

    public function getType(): TypeEnum
    {
        return TypeEnum::STRING;
    }

    public function getValue(): int
    {
        return $this->maxVideoPreview;
    }

    public function setMaxVideoPreview(int $maxVideoPreview): MaxVideoPreview
    {
        $this->maxVideoPreview = $maxVideoPreview;
        return $this;
    }

    public function setMaxVideoPreviewNone(): MaxVideoPreview
    {
        $this->maxVideoPreview = 0;

        return $this;
    }

    public function setMaxVideoPreviewNoLimit(): MaxVideoPreview
    {
        $this->maxVideoPreview = -1;

        return $this;
    }
}
