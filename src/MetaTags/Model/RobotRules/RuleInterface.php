<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Interface RobotRuleInterface
 *
 * @author Benjamin Georgeault
 */
interface RuleInterface
{
    public function getName(): string;

    public function getType(): TypeEnum;

    public function getValue(): mixed;
}
