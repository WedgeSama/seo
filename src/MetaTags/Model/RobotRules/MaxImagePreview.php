<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class MaxImagePreview
 *
 * @author Benjamin Georgeault
 */
final class MaxImagePreview implements RuleInterface
{
    private MaxImagePreviewEnum $maxImagePreview = MaxImagePreviewEnum::STANDARD;

    public static function create(MaxImagePreviewEnum $maxImagePreview = MaxImagePreviewEnum::STANDARD): self
    {
        return (new self())
            ->setMaxImagePreview($maxImagePreview)
        ;
    }

    public function getName(): string
    {
        return 'max-image-preview';
    }

    public function getType(): TypeEnum
    {
        return TypeEnum::STRING;
    }

    public function getValue(): string
    {
        return $this->maxImagePreview->value;
    }

    public function setMaxImagePreview(MaxImagePreviewEnum $maxImagePreview): MaxImagePreview
    {
        $this->maxImagePreview = $maxImagePreview;
        return $this;
    }
}
