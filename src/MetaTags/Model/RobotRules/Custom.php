<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class Custom
 *
 * @author Benjamin Georgeault
 */
final class Custom implements RuleInterface
{
    private string $name = 'custom';
    private mixed $value = null;
    private TypeEnum $type = TypeEnum::STRING;

    public static function create(string $name, mixed $value, TypeEnum $type = TypeEnum::STRING): Custom
    {
        return (new Custom())
            ->setName($name)
            ->setValue($value)
            ->setType($type)
        ;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): TypeEnum
    {
        return $this->type;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function setName(string $name): Custom
    {
        $this->name = $name;
        return $this;
    }

    public function setValue(mixed $value): Custom
    {
        $this->value = $value;
        return $this;
    }

    public function setType(TypeEnum $type): Custom
    {
        $this->type = $type;
        return $this;
    }
}
