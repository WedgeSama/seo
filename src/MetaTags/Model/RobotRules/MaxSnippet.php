<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model\RobotRules;

/**
 * Class MaxSnippet
 *
 * @author Benjamin Georgeault
 */
final class MaxSnippet implements RuleInterface
{
    /**
     * Default -1. Google will choose the snippet length that it believes is most effective to help users discover
     * your content and direct users to your site.
     *
     * @see https://developers.google.com/search/docs/crawling-indexing/robots-meta-tag#directives
     */
    private int $maxSnippet = -1;

    public static function create(int $maxSnippet): MaxSnippet
    {
        return (new self())
            ->setMaxSnippet($maxSnippet)
        ;
    }

    public function getName(): string
    {
        return 'max-snippet';
    }

    public function getType(): TypeEnum
    {
        return TypeEnum::INT;
    }

    public function getValue(): int
    {
        return $this->maxSnippet;
    }

    public function setMaxSnippet(int $maxSnippet): MaxSnippet
    {
        $this->maxSnippet = $maxSnippet;

        return $this;
    }

    public function setNoSnippet(): MaxSnippet
    {
        $this->maxSnippet = 0;

        return $this;
    }

    public function setMaxSnippetDefault(): MaxSnippet
    {
        $this->maxSnippet = -1;

        return $this;
    }
}
