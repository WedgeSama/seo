<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model;

/**
 * Class Description
 *
 * @author Benjamin Georgeault
 */
final class Description implements MetaTagInterface
{
    private ?string $description = null;

    public static function create(string $description): Description
    {
        return (new Description())
            ->setDescription($description)
        ;
    }

    public function getName(): string
    {
        return 'description';
    }

    public function getContent(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Description
    {
        $this->description = $description;
        return $this;
    }
}
