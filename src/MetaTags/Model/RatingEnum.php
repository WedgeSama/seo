<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Model;

enum RatingEnum: string
{
    case GENERAL = 'general';

    case SAFE_FOR_KIDS = 'safe for kids';

    case RESTRICTED = 'restricted';

    case MATURE = 'mature';

    case ADULT = 'adult';
}
