<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Serializer;

/**
 * Interface SerializerInterface
 *
 * @author Benjamin Georgeault
 */
interface SerializerInterface
{
    public function serialize(iterable $metaTags): array;

    public function deserialize(array $data): iterable;
}
