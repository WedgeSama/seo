<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\MetaTags\Generator;

use WS\Library\Seo\MetaTags\Model\MetaTagBagInterface;

/**
 * Class Generator
 *
 * @author Benjamin Georgeault
 */
final class Generator implements GeneratorInterface
{
    public function generate(MetaTagBagInterface $metaTagBag): string
    {
        $output = '';

        foreach ($metaTagBag->getMetaTags() as $metaTag) {
            $output .= sprintf('<meta name="%s" content="%s">', $metaTag->getName(), $metaTag->getContent())."\n";
        }

        return $output;
    }
}
