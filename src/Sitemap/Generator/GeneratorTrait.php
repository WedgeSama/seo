<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Generator;

/**
 * Trait GeneratorTrait
 *
 * @author Benjamin Georgeault
 */
trait GeneratorTrait
{
    private function fixNamespacedName(string $name): string
    {
        if (str_contains($name, ':')) {
            $name = 'xmlns:' . $name;
        }

        return $name;
    }
}
