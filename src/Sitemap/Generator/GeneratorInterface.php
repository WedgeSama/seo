<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Generator;

use WS\Library\Seo\Sitemap\Model\SiteMapIndexInterface;
use WS\Library\Seo\Sitemap\Model\UrlSetInterface;

/**
 * Interface GeneratorInterface
 *
 * @author Benjamin Georgeault
 */
interface GeneratorInterface
{
    public function generate(UrlSetInterface|SiteMapIndexInterface $root, ?string $fileName = null): string|bool;
}
