<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Generator;

use WS\Library\Seo\Sitemap\Exception\GeneratorException;
use WS\Library\Seo\Sitemap\Exception\InvalidArgumentException;
use WS\Library\Seo\Sitemap\Exception\UniqueElementException;
use WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler\AlternateLanguageCompiler;
use WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler\ImageCompiler;
use WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler\NewsCompiler;
use WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler\UrlAdditionalInfoCompilerInterface;
use WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler\VideoCompiler;
use WS\Library\Seo\Sitemap\Model\SiteMapIndexInterface;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\UrlAdditionalInfoInterface;
use WS\Library\Seo\Sitemap\Model\UrlInterface;
use WS\Library\Seo\Sitemap\Model\UrlSetInterface;

/**
 * Class Generator
 *
 * @author Benjamin Georgeault
 */
final class Generator implements GeneratorInterface
{
    use GeneratorTrait;

    private array $internalUrlAdditionalInfoCompilers;

    public function __construct(
        /** @var iterable<UrlAdditionalInfoCompilerInterface> */
        private readonly iterable $urlAdditionalInfoCompilers = [],
    ) {
        foreach ($this->urlAdditionalInfoCompilers as $key => $compiler) {
            if (!is_object($compiler)) {
                throw new InvalidArgumentException(sprintf(
                    'Compiler must be an object, %s given for key %s',
                    gettype($compiler),
                    $key,
                ));
            }

            if (!$compiler instanceof UrlAdditionalInfoCompilerInterface) {
                throw new InvalidArgumentException(sprintf(
                    'Compiler %s must implement %s',
                    $compiler::class,
                    UrlAdditionalInfoCompilerInterface::class,
                ));
            }
        }

        $this->internalUrlAdditionalInfoCompilers = [
            new AlternateLanguageCompiler(),
            new NewsCompiler(),
            new ImageCompiler(),
            new VideoCompiler(),
        ];
    }

    public function generate(UrlSetInterface|SiteMapIndexInterface $root, ?string $fileName = null): string|bool
    {
        if ($root instanceof SiteMapIndexInterface) {
            return $this->generateSiteMapIndex($root, $fileName);
        }

        return $this->generateUrlSet($root, $fileName);
    }

    private function generateSiteMapIndex(SiteMapIndexInterface $root, ?string $fileName = null): string|bool
    {
        $xml = new \SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8" ?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>',
        );

        foreach ($root->getSiteMaps() as $siteMap) {
            $sitemap = $xml->addChild('sitemap');
            $sitemap->addChild('loc', $siteMap->getLoc());

            if (null !== $lastMod = $siteMap->getLastMod()) {
                $sitemap->addChild('lastmod', $lastMod->format('c'));
            }
        }

        return $xml->asXML($fileName);
    }

    private function generateUrlSet(UrlSetInterface $root, ?string $fileName = null): string|bool
    {
        $xml = new \SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8" ?><urlset></urlset>',
        );

        foreach ($root->getUrls() as $url) {
            if (!$url instanceof UrlInterface) {
                throw new GeneratorException(sprintf(
                    'Url must be an instance of %s',
                    UrlInterface::class,
                ));
            }

            $urlElement = $xml->addChild('url');
            $urlElement->addChild('loc', $url->getLoc());

            if (null !== $lastMod = $url->getLastMod()) {
                $urlElement->addChild('lastmod', $lastMod->format('c'));
            }

            if (null !== $changeFreq = $url->getChangeFreq()) {
                $urlElement->addChild('changefreq', $changeFreq->value);
            }

            if (null !== $priority = $url->getPriority()) {
                $urlElement->addChild('priority', number_format(
                    min(1.0, max(0.0, $priority)),
                    1,
                ));
            }

            if (null !== $additionalInfo = $url->getAdditionalInfo()) {
                foreach ($additionalInfo as $info) {
                    if (!$info instanceof UrlAdditionalInfoInterface) {
                        throw new GeneratorException(sprintf(
                            'Additional info must be an instance of %s',
                            UrlAdditionalInfoInterface::class,
                        ));
                    }

                    if ($info->isUnique() && $this->checkIfElemHasChild($urlElement, $info)) {
                        throw new UniqueElementException(sprintf(
                            'Element %s already exists in Url "%s"',
                            $info->getName(),
                            $url->getLoc(),
                        ));
                    }

                    $this->findCompiler($info)->compile(
                        $urlElement->addChild($this->fixNamespacedName($info->getName())),
                        $info,
                        $url,
                        $root,
                    );
                }
            }
        }

        foreach ($root->getAttributes() as $attr => $value) {
            $xml->addAttribute($this->fixNamespacedName($attr), $value);
        }

        return $xml->asXML($fileName);
    }

    private function findCompiler(UrlAdditionalInfoInterface $info): UrlAdditionalInfoCompilerInterface
    {
        foreach ($this->getUrlAdditionalInfoCompilers() as $compiler) {
            if ($compiler->supports($info)) {
                return $compiler;
            }
        }

        throw new GeneratorException(sprintf('No compiler found for %s', get_class($info)));
    }

    /**
     * @return iterable<UrlAdditionalInfoCompilerInterface>
     */
    private function getUrlAdditionalInfoCompilers(): iterable
    {
        foreach ($this->internalUrlAdditionalInfoCompilers as $compiler) {
            yield $compiler;
        }

        foreach ($this->urlAdditionalInfoCompilers as $compiler) {
            yield $compiler;
        }
    }

    private function checkIfElemHasChild(\SimpleXMLElement $element, UrlAdditionalInfoInterface $info): bool
    {
        foreach ($element->children() as $child) {
            if ($child->getName() === $info->getName()) {
                return true;
            }
        }

        return false;
    }
}
