<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler;

use WS\Library\Seo\Sitemap\Generator\GeneratorTrait;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\News;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\UrlAdditionalInfoInterface;
use WS\Library\Seo\Sitemap\Model\UrlInterface;
use WS\Library\Seo\Sitemap\Model\UrlSetInterface;

/**
 * Class NewsCompiler
 *
 * @author Benjamin Georgeault
 */
class NewsCompiler implements UrlAdditionalInfoCompilerInterface
{
    use GeneratorTrait;

    /**
     * @param News $info
     */
    public function compile(\SimpleXMLElement $element, UrlAdditionalInfoInterface $info, UrlInterface $url, UrlSetInterface $urlSet): \SimpleXMLElement
    {
        $urlSet->addAttribute('xmlns:news', 'http://www.google.com/schemas/sitemap-news/0.9');

        $publicationElement = $element->addChild($this->fixNamespacedName('news:publication'));
        $publicationElement->addChild($this->fixNamespacedName('news:name'), $info->getPublisherName());
        $publicationElement->addChild($this->fixNamespacedName('news:language'), $info->getLanguage());

        $element->addChild($this->fixNamespacedName('news:publication_date'), $info->getPublicationDate()->format('c'));
        $element->addChild($this->fixNamespacedName('news:title'), $info->getTitle());

        return $element;
    }

    public function supports(UrlAdditionalInfoInterface $info): bool
    {
        return $info instanceof News;
    }
}
