<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler;

use WS\Library\Seo\Sitemap\Generator\GeneratorTrait;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\Image;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\UrlAdditionalInfoInterface;
use WS\Library\Seo\Sitemap\Model\UrlInterface;
use WS\Library\Seo\Sitemap\Model\UrlSetInterface;

/**
 * Class ImageCompiler
 *
 * @author Benjamin Georgeault
 */
class ImageCompiler implements UrlAdditionalInfoCompilerInterface
{
    use GeneratorTrait;

    /**
     * @param Image $info
     */
    public function compile(\SimpleXMLElement $element, UrlAdditionalInfoInterface $info, UrlInterface $url, UrlSetInterface $urlSet): \SimpleXMLElement
    {
        $urlSet->addAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');

        $element->addChild($this->fixNamespacedName('image:loc'), $info->getLoc());

        return $element;
    }

    public function supports(UrlAdditionalInfoInterface $info): bool
    {
        return $info instanceof Image;
    }
}
