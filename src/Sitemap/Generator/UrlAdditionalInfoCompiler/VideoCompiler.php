<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler;

use WS\Library\Seo\Sitemap\Generator\GeneratorTrait;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\UrlAdditionalInfoInterface;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\Video;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\VideoPlatformEnum;
use WS\Library\Seo\Sitemap\Model\UrlInterface;
use WS\Library\Seo\Sitemap\Model\UrlSetInterface;

/**
 * Class VideoCompiler
 *
 * @author Benjamin Georgeault
 */
class VideoCompiler implements UrlAdditionalInfoCompilerInterface
{
    use GeneratorTrait;
    
    /**
     * @param Video $info
     */
    public function compile(\SimpleXMLElement $element, UrlAdditionalInfoInterface $info, UrlInterface $url, UrlSetInterface $urlSet): \SimpleXMLElement
    {
        $urlSet->addAttribute('xmlns:video', 'http://www.google.com/schemas/sitemap-video/1.1');

        $element->addChild($this->fixNamespacedName('video:thumbnail_loc'), $info->getThumbnailLoc());
        $element->addChild($this->fixNamespacedName('video:title'), $info->getTitle());
        $element->addChild($this->fixNamespacedName('video:description'), $info->getDescription());

        $element->addChild(
            $this->fixNamespacedName(sprintf('video:%s', $info->getLocType()->value)),
            $info->getLoc(),
        );

        if (null !== $duration = $info->getDuration()) {
            $element->addChild($this->fixNamespacedName('video:duration'), min(28800, max(1, $duration)));
        }

        if (null !== $expirationDate = $info->getExpirationDate()) {
            $element->addChild($this->fixNamespacedName('video:expiration_date'), $expirationDate->format('c'));
        }

        if (null !== $rating = $info->getRating()) {
            $element->addChild($this->fixNamespacedName('video:rating'), number_format(
                min(5.0, max(0.0, $rating)),
                1,
            ));
        }

        if (null !== $viewCount = $info->getViewCount()) {
            $element->addChild($this->fixNamespacedName('video:view_count'), $viewCount);
        }

        if (null !== $publicationDate = $info->getPublicationDate()) {
            $element->addChild($this->fixNamespacedName('video:publication_date'), $publicationDate->format('c'));
        }

        if (null !== $familyFriendly = $info->getFamilyFriendly()) {
            $element->addChild($this->fixNamespacedName('video:family_friendly'), $familyFriendly ? 'yes' : 'no');
        }

        if (null !== $restriction = $info->getRestriction()) {
            $restrictionElement = $element->addChild($this->fixNamespacedName('video:restriction'), implode(' ', $restriction));
            $restrictionElement->addAttribute('relationship', $info->getRestrictionRelationship()->value);
        }

        if (null !== $platform = $info->getPlatform()) {
            $platformElement = $element->addChild(
                $this->fixNamespacedName('video:platform'),
                implode(' ', array_map(function (VideoPlatformEnum $pl) {
                    return $pl->value;
                }, iterator_to_array($platform))),
            );

            $platformElement->addAttribute('relationship', $info->getPlatformRelationship()->value);
        }

        if (null !== $subscription = $info->getRequiresSubscription()) {
            $element->addChild($this->fixNamespacedName('video:requires_subscription'), $subscription ? 'yes' : 'no');
        }

        if (null !== $uploader = $info->getUploader()) {
            $uploaderElement = $element->addChild($this->fixNamespacedName('video:uploader'), $uploader);

            if (null !== $uploaderInfo = $info->getUploaderInfo()) {
                $uploaderElement->addAttribute('info', $uploaderInfo);
            }
        }

        if (null !== $live = $info->getLive()) {
            $element->addChild($this->fixNamespacedName('video:live'), $live ? 'yes' : 'no');
        }

        if (null !== $tag = $info->getTag()) {
            $element->addChild($this->fixNamespacedName('video:tag'), $tag);
        }

        return $element;
    }

    public function supports(UrlAdditionalInfoInterface $info): bool
    {
        return $info instanceof Video;
    }
}
