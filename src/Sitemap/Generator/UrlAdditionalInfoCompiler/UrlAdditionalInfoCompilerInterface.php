<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Generator\UrlAdditionalInfoCompiler;

use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\UrlAdditionalInfoInterface;
use WS\Library\Seo\Sitemap\Model\UrlInterface;
use WS\Library\Seo\Sitemap\Model\UrlSetInterface;

/**
 * Interface UrlAdditionalInfoCompilerInterface
 *
 * @author Benjamin Georgeault
 */
interface UrlAdditionalInfoCompilerInterface
{
    public function compile(
        \SimpleXMLElement $element,
        UrlAdditionalInfoInterface $info,
        UrlInterface $url,
        UrlSetInterface $urlSet,
    ): \SimpleXMLElement;

    public function supports(UrlAdditionalInfoInterface $info): bool;
}
