<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Exception;

/**
 * All exception in the sitemap part must implement this interface.
 *
 * @author Benjamin Georgeault
 */
interface SitemapExceptionInterface extends \Throwable
{
}
