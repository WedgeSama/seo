<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model;

/**
 * Interface UrlSetInterface
 *
 * @author Benjamin Georgeault
 */
interface UrlSetInterface
{
    /**
     * @return iterable<UrlInterface>
     */
    public function getUrls(): iterable;

    /**
     * @return iterable<string, string>
     */
    public function getAttributes(): iterable;

    public function addAttribute(string $attr, string $value): static;
}
