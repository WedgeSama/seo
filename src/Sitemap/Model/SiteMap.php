<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model;

/**
 * Class SiteMap
 *
 * @author Benjamin Georgeault
 */
final class SiteMap implements SiteMapInterface
{
    private string $loc;

    private ?\DateTimeInterface $lastMod = null;

    public static function create(
        string $loc,
        ?\DateTimeInterface $lastMod = null,
    ): SiteMap {
        return (new self())
            ->setLoc($loc)
            ->setLastMod($lastMod)
        ;
    }

    public function getLoc(): string
    {
        return $this->loc;
    }

    public function setLoc(string $loc): SiteMap
    {
        $this->loc = $loc;
        return $this;
    }

    public function getLastMod(): ?\DateTimeInterface
    {
        return $this->lastMod;
    }

    public function setLastMod(?\DateTimeInterface $lastMod): SiteMap
    {
        $this->lastMod = $lastMod;
        return $this;
    }
}
