<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model;

/**
 * Class Url
 *
 * @author Benjamin Georgeault
 */
final class Url implements UrlInterface
{
    private string $loc;

    private ?\DateTimeInterface $lastMod = null;

    private ?ChangeFreqEnum $changeFreq = null;

    private ?float $priority = null;

    private ?iterable $additionalInfo = null;

    public static function create(
        string $loc,
        ?\DateTimeInterface $lastMod = null,
        ?ChangeFreqEnum $changeFreq = null,
        ?float $priority = null,
        ?iterable $additionalInfo = null,
    ): Url {
        return (new self())
            ->setLoc($loc)
            ->setLastMod($lastMod)
            ->setChangeFreq($changeFreq)
            ->setPriority($priority)
            ->setAdditionalInfo($additionalInfo)
        ;
    }

    public function getLoc(): string
    {
        return $this->loc;
    }

    public function setLoc(string $loc): Url
    {
        $this->loc = $loc;
        return $this;
    }

    public function getLastMod(): ?\DateTimeInterface
    {
        return $this->lastMod;
    }

    public function setLastMod(?\DateTimeInterface $lastMod): Url
    {
        $this->lastMod = $lastMod;
        return $this;
    }

    public function getChangeFreq(): ?ChangeFreqEnum
    {
        return $this->changeFreq;
    }

    public function setChangeFreq(?ChangeFreqEnum $changeFreq): Url
    {
        $this->changeFreq = $changeFreq;
        return $this;
    }

    public function getPriority(): ?float
    {
        return $this->priority;
    }

    public function setPriority(?float $priority): Url
    {
        $this->priority = $priority;
        return $this;
    }

    public function getAdditionalInfo(): ?iterable
    {
        return $this->additionalInfo;
    }

    /**
     * @param iterable<UrlAdditionalInfo\UrlAdditionalInfoInterface>|null $additionalInfo
     */
    public function setAdditionalInfo(?iterable $additionalInfo): Url
    {
        $this->additionalInfo = $additionalInfo;
        return $this;
    }
}
