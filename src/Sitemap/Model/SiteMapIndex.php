<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model;

/**
 * Class SiteMapIndex
 *
 * @author Benjamin Georgeault
 */
final class SiteMapIndex implements SiteMapIndexInterface
{
    /** @var iterable<SiteMapInterface> */
    private iterable $siteMaps = [];

    public static function create(iterable $siteMaps): SiteMapIndex
    {
        return (new self())
            ->setSiteMaps($siteMaps)
        ;
    }

    public function getSiteMaps(): iterable
    {
        return $this->siteMaps;
    }

    public function setSiteMaps(iterable $siteMaps): SiteMapIndex
    {
        $this->siteMaps = $siteMaps;
        return $this;
    }
}
