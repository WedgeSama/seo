<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo;

enum VideoPlatformEnum: string
{
    case WEB = 'web';

    case MOBILE = 'mobile';

    case TV = 'tv';
}
