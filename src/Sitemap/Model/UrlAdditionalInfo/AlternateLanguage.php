<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo;

/**
 * Class AlternateLanguage
 *
 * @author Benjamin Georgeault
 */
final class AlternateLanguage implements UrlAdditionalInfoInterface
{
    private string $href;

    private string $hrefLang;

    public static function create(string $hrefLang, string $href): AlternateLanguage
    {
        return (new self())
            ->setHref($href)
            ->setHrefLang($hrefLang)
        ;
    }

    public function getName(): string
    {
        return 'xhtml:link';
    }

    public function isUnique(): bool
    {
        return false;
    }

    public function getHref(): string
    {
        return $this->href;
    }

    public function setHref(string $href): AlternateLanguage
    {
        $this->href = $href;
        return $this;
    }

    public function getHrefLang(): string
    {
        return $this->hrefLang;
    }

    public function setHrefLang(string $hrefLang): AlternateLanguage
    {
        $this->hrefLang = $hrefLang;
        return $this;
    }
}
