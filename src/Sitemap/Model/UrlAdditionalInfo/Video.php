<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo;

/**
 * Class Video
 *
 * @author Benjamin Georgeault
 */
final class Video implements UrlAdditionalInfoInterface
{
    // Required properties
    private string $thumbnailLoc;

    private string $title;

    private string $description;

    private string $loc;

    private VideoLocTypeEnum $locType = VideoLocTypeEnum::CONTENT;

    // Optional properties
    private ?int $duration = null;

    private ?\DateTimeInterface $expirationDate = null;

    private ?float $rating = null;

    private ?int $viewCount = null;

    private ?\DateTimeInterface $publicationDate;

    private ?bool $familyFriendly = null;

    /**
     * Specify a space-delimited list of country codes in ISO 3166 format.
     * Eg:
     *  - 2 letters (en, fr, de, etc...)
     *  - 3 letters (eng, fre, deu, etc...)
     */
    private ?iterable $restriction = null;

    private ?VideoRelationshipEnum $restrictionRelationship = null;

    /** @var iterable<VideoPlatformEnum>|null */
    private ?iterable $platform = null;

    private ?VideoRelationshipEnum $platformRelationship = null;

    private ?bool $requiresSubscription = null;

    private ?string $uploader = null;

    private ?string $uploaderInfo = null;

    private ?bool $live = null;

    private ?string $tag = null;

    public static function create(
        string $thumbnailLoc,
        string $title,
        string $description,
        string $loc,
        VideoLocTypeEnum $locType = VideoLocTypeEnum::CONTENT,
        ?int $duration = null,
        ?\DateTimeInterface $expirationDate = null,
        ?float $rating = null,
        ?int $viewCount = null,
        ?\DateTimeInterface $publicationDate = null,
        ?bool $familyFriendly = null,
        ?iterable $restriction = null,
        VideoRelationshipEnum $restrictionRelationship = VideoRelationshipEnum::ALLOW,
        ?iterable $platform = null,
        ?VideoRelationshipEnum $platformRelationship = VideoRelationshipEnum::ALLOW,
        ?bool $requiresSubscription = null,
        ?string $uploader = null,
        ?string $uploaderInfo = null,
        ?bool $live = null,
        ?string $tag = null,
    ): Video {
        $video = (new self())
            ->setThumbnailLoc($thumbnailLoc)
            ->setTitle($title)
            ->setDescription($description)
            ->setLoc($loc)
            ->setLocType($locType)
            ->setDuration($duration)
            ->setExpirationDate($expirationDate)
            ->setRating($rating)
            ->setViewCount($viewCount)
            ->setPublicationDate($publicationDate)
            ->setFamilyFriendly($familyFriendly)
            ->setRequiresSubscription($requiresSubscription)
            ->setRequiresSubscription($live)
            ->setRequiresSubscription($tag)
        ;

        if (null !== $restriction) {
            $video->setRestriction($restriction, $restrictionRelationship);
        }

        if (null !== $platform) {
            $video->setPlatform($platform, $platformRelationship);
        }

        if (null !== $uploader) {
            $video->setUploader($uploader, $uploaderInfo);
        }

        return $video;
    }

    public function getName(): string
    {
        return 'video:video';
    }

    public function isUnique(): bool
    {
        return false;
    }

    public function getThumbnailLoc(): string
    {
        return $this->thumbnailLoc;
    }

    public function setThumbnailLoc(string $thumbnailLoc): Video
    {
        $this->thumbnailLoc = $thumbnailLoc;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Video
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Video
    {
        $this->description = $description;
        return $this;
    }

    public function getLoc(): string
    {
        return $this->loc;
    }

    public function setLoc(string $loc): Video
    {
        $this->loc = $loc;
        return $this;
    }

    public function getLocType(): VideoLocTypeEnum
    {
        return $this->locType;
    }

    public function setLocType(VideoLocTypeEnum $locType): Video
    {
        $this->locType = $locType;
        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): Video
    {
        $this->duration = $duration;
        return $this;
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(?\DateTimeInterface $expirationDate): Video
    {
        $this->expirationDate = $expirationDate;
        return $this;
    }

    public function getRating(): ?float
    {
        return $this->rating;
    }

    public function setRating(?float $rating): Video
    {
        $this->rating = $rating;
        return $this;
    }

    public function getViewCount(): ?int
    {
        return $this->viewCount;
    }

    public function setViewCount(?int $viewCount): Video
    {
        $this->viewCount = $viewCount;
        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(?\DateTimeInterface $publicationDate): Video
    {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    public function getFamilyFriendly(): ?bool
    {
        return $this->familyFriendly;
    }

    public function setFamilyFriendly(?bool $familyFriendly): Video
    {
        $this->familyFriendly = $familyFriendly;
        return $this;
    }

    public function getRestriction(): ?iterable
    {
        return $this->restriction;
    }

    public function removeRestriction(): Video
    {
        $this->restriction = null;
        $this->restrictionRelationship = null;
        return $this;
    }

    public function setRestriction(iterable $countries, VideoRelationshipEnum $relationship = VideoRelationshipEnum::ALLOW): Video
    {
        $this->restriction = $countries;
        $this->restrictionRelationship = $relationship;
        return $this;
    }

    public function getRestrictionRelationship(): ?VideoRelationshipEnum
    {
        return $this->restrictionRelationship;
    }

    public function getPlatform(): ?iterable
    {
        return $this->platform;
    }

    public function removePlatform(): Video
    {
        $this->platform = null;
        $this->platformRelationship = null;
        return $this;
    }

    /**
     * @param iterable<VideoPlatformEnum> $platform
     */
    public function setPlatform(iterable $platform, VideoRelationshipEnum $relationship = VideoRelationshipEnum::ALLOW): Video
    {
        $this->platform = $platform;
        $this->platformRelationship = $relationship;
        return $this;
    }

    public function getPlatformRelationship(): ?VideoRelationshipEnum
    {
        return $this->platformRelationship;
    }

    public function getRequiresSubscription(): ?bool
    {
        return $this->requiresSubscription;
    }

    public function setRequiresSubscription(?bool $requiresSubscription): Video
    {
        $this->requiresSubscription = $requiresSubscription;
        return $this;
    }

    public function getUploader(): ?string
    {
        return $this->uploader;
    }

    public function setUploader(?string $uploader, ?string $uploaderInfo = null): Video
    {
        $this->uploader = $uploader;
        $this->uploaderInfo = $uploaderInfo;
        return $this;
    }

    public function getUploaderInfo(): ?string
    {
        return $this->uploaderInfo;
    }

    public function getLive(): ?bool
    {
        return $this->live;
    }

    public function setLive(?bool $live): Video
    {
        $this->live = $live;
        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(?string $tag): Video
    {
        $this->tag = $tag;
        return $this;
    }
}
