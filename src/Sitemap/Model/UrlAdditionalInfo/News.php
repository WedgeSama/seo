<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo;

/**
 * Class News
 *
 * @author Benjamin Georgeault
 */
final class News implements UrlAdditionalInfoInterface
{
    private string $title;

    private string $publisherName;

    /**
     * ISO 639 language code.
     * Eg:
     *  - 2 letters (en, fr, de, etc...)
     *  - 3 letters (eng, fre, deu, etc...)
     */
    private string $language;

    private \DateTimeInterface $publicationDate;

    public static function create(
        string $title,
        string $publisherName,
        string $language,
        \DateTimeInterface $publicationDate,
    ): News {
        return (new self())
            ->setTitle($title)
            ->setPublisherName($publisherName)
            ->setLanguage($language)
            ->setPublicationDate($publicationDate)
        ;
    }

    public function getName(): string
    {
        return 'news:news';
    }

    public function isUnique(): bool
    {
        return true;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): News
    {
        $this->title = $title;
        return $this;
    }

    public function getPublisherName(): string
    {
        return $this->publisherName;
    }

    public function setPublisherName(string $publisherName): News
    {
        $this->publisherName = $publisherName;
        return $this;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): News
    {
        $this->language = $language;
        return $this;
    }

    public function getPublicationDate(): \DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $publicationDate): News
    {
        $this->publicationDate = $publicationDate;
        return $this;
    }
}
