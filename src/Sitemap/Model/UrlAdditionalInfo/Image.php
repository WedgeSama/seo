<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo;

/**
 * Class Image
 *
 * @author Benjamin Georgeault
 */
final class Image implements UrlAdditionalInfoInterface
{
    private string $loc;

    public static function create(string $loc): Image
    {
        return (new self())
            ->setLoc($loc)
        ;
    }

    public function getName(): string
    {
        return 'image:image';
    }

    public function isUnique(): bool
    {
        return false;
    }

    public function getLoc(): string
    {
        return $this->loc;
    }

    public function setLoc(string $loc): Image
    {
        $this->loc = $loc;
        return $this;
    }
}
