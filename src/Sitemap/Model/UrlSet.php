<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model;

use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\Image;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\News;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\Video;

/**
 * Class UrlSet
 *
 * @author Benjamin Georgeault
 */
final class UrlSet implements UrlSetInterface
{
    private ?iterable $urls = null;

    private array $attributes = [
        'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd',
    ];

    public static function create(iterable $urls, array $additionalAttrs = []): UrlSet
    {
        return (new self())
            ->setUrls($urls)
            ->addAttributes($additionalAttrs)
        ;
    }

    public function getUrls(): iterable
    {
        return $this->urls ?? [];
    }

    public function setUrls(iterable $urls): UrlSet
    {
        $this->urls = $urls;
        return $this;
    }

    public function getAttributes(): iterable
    {
        return $this->attributes;
    }

    public function addAttributes(array $attributes): UrlSet
    {
        foreach ($attributes as $attr => $value) {
            $this->addAttribute($attr, $value);
        }

        return $this;
    }

    public function addAttribute(string $attr, string $value): static
    {
        if (!array_key_exists($attr, $this->attributes)) {
            $this->attributes[$attr] = $value;
        }

        return $this;
    }

    public function addVideo(): UrlSet
    {
        $this->addAttribute('xmlns:video', 'http://www.google.com/schemas/sitemap-video/1.1');
        return $this;
    }
}
