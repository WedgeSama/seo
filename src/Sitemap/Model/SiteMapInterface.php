<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model;

/**
 * Interface SiteMapInterface
 *
 * @author Benjamin Georgeault
 */
interface SiteMapInterface
{
    public function getLoc(): ?string;

    public function getLastMod(): ?\DateTimeInterface;
}
