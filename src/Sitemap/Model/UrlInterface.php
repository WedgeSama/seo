<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Sitemap\Model;

/**
 * Interface UrlInterface
 *
 * @author Benjamin Georgeault
 */
interface UrlInterface
{
    public function getLoc(): ?string;

    public function getLastMod(): ?\DateTimeInterface;

    public function getChangeFreq(): ?ChangeFreqEnum;

    public function getPriority(): ?float;

    /**
     * @return iterable<UrlAdditionalInfo\UrlAdditionalInfoInterface>
     */
    public function getAdditionalInfo(): ?iterable;
}
