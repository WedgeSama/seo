<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Symfony\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        ($treeBuilder = new TreeBuilder('ws_seo'))->getRootNode()
            ->children()
                ->append($this->metatagNode())
            ->end()
        ;

        return $treeBuilder;
    }

    private function metatagNode(): NodeDefinition
    {
        return (new TreeBuilder('metatags'))->getRootNode()
            ->canBeDisabled()
        ;
    }
}
