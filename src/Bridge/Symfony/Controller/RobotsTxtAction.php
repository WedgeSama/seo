<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Symfony\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use WS\Library\Seo\RobotsTxt\Generator\GeneratorInterface;
use WS\Library\Seo\RobotsTxt\Loader\LoaderInterface;

/**
 * Class RobotsTxtAction
 *
 * @author Benjamin Georgeault
 */
readonly class RobotsTxtAction
{
    public function __construct(
        private GeneratorInterface $generator,
        private LoaderInterface $loader,
    ) {}

    #[Route('/robots.txt', name: 'ws_seo_robots_txt', methods: 'GET')]
    public function __invoke(Request $request): Response
    {
        return new Response(
            $this->generator->generate(
                $this->loader->load($request->getHost()),
            ),
        );
    }
}
