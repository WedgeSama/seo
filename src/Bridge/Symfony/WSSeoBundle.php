<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Symfony;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class WSSeoBundle
 *
 * @author Benjamin Georgeault
 */
class WSSeoBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        if (class_exists(DoctrineOrmMappingsPass::class)) {
            $container->addCompilerPass(DoctrineOrmMappingsPass::createAttributeMappingDriver([
                'WS\Library\Seo\Bridge\Doctrine\ORM\Entity',
            ], [
                realpath(__DIR__ . '/../Doctrine/ORM/Entity'),
            ],
                enabledParameter: 'ws_seo.metatags.doctrine_orm.enabled',
                reportFieldsWhereDeclared: true,
            ));
        }
    }
}
