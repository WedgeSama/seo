<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Symfony\MetaTags\Serializer;

use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;
use WS\Library\Seo\MetaTags\Serializer\SerializerInterface;

/**
 * Class Serializer
 *
 * @author Benjamin Georgeault
 */
final readonly class Serializer implements SerializerInterface
{
    public function __construct(
        private SymfonySerializerInterface $serializer,
    ) {}

    public function serialize(iterable $metaTags): array
    {
        return json_decode($this->serializer->serialize($metaTags, 'json', [
            'ws_seo_serialization' => true,
        ]), true);
    }

    public function deserialize(array $data): iterable
    {
        return $this->serializer->deserialize(json_encode($data), 'array', 'json', [
            'ws_seo_serialization' => true,
        ]);
    }
}
