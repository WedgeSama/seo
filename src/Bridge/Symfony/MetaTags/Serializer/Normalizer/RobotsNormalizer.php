<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Symfony\MetaTags\Serializer\Normalizer;

use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use WS\Library\Seo\MetaTags\Model\RobotRules\RuleInterface;
use WS\Library\Seo\MetaTags\Model\Robots;

/**
 * Class RobotsNormalizer
 *
 * @author Benjamin Georgeault
 */
class RobotsNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function denormalize(mixed $data, string $type, ?string $format = null, array $context = []): Robots
    {
        $rules = [];
        foreach ($data['robotRules'] as $rule) {
            $rules[] = $this->serializer->denormalize($rule['data'], $rule['class'], $format, $context);
        }

        return Robots::create($rules, $data['name']);
    }

    public function supportsDenormalization(mixed $data, string $type, ?string $format = null, array $context = []): bool
    {
        return $type === Robots::class;
    }

    public function normalize(mixed $object, ?string $format = null, array $context = []): array
    {
        if (!$object instanceof Robots) {
            throw new InvalidArgumentException(sprintf(
                'The object must be an instance of "%s".',
                Robots::class,
            ));
        }

        $data = [
            'name' => $object->getName(),
        ];

        if (null !== $rules = $object->getRobotRules()) {
            $data['robotRules'] = array_map(function (RuleInterface $rule) {
                return [
                    'class' => get_class($rule),
                    'data' => $this->serializer->normalize($rule),
                ];
            }, iterator_to_array($rules));
        }

        return $data;
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof Robots;
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            Robots::class => true,
        ];
    }
}
