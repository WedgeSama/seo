<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Symfony\MetaTags\Serializer\Normalizer;

use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use WS\Library\Seo\MetaTags\Model\MetaTagInterface;

/**
 * Class MetaTagNormalizer
 *
 * @author Benjamin Georgeault
 */
class MetaTagNormalizer implements NormalizerInterface, DenormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function denormalize(mixed $data, string $type, ?string $format = null, array $context = []): MetaTagInterface
    {
        $class = $data['class'];

        if (!is_subclass_of($class, MetaTagInterface::class)) {
            throw new InvalidArgumentException(sprintf(
                'The class "%s" must implement the "%s".',
                $class,
                MetaTagInterface::class,
            ));
        }

        unset($context['ws_seo_serialization']);

        return $this->serializer->denormalize($data['data'] ?? [], $class, $format, $context);
    }

    public function supportsDenormalization(mixed $data, string $type, ?string $format = null, array $context = []): bool
    {
        return array_key_exists('ws_seo_serialization', $context)
            && is_array($data)
            && array_key_exists('class', $data)
            && is_subclass_of($data['class'], MetaTagInterface::class)
            && 'json' === $format
        ;
    }

    public function normalize(mixed $object, ?string $format = null, array $context = []): array
    {
        if (!$object instanceof MetaTagInterface) {
            throw new InvalidArgumentException(sprintf(
                'The object must implement the "%s".',
                MetaTagInterface::class,
            ));
        }

        unset($context['ws_seo_serialization']);

        $array = [
            'class' => get_class($object),
        ];

        $data = $this->serializer->normalize($object, $format, $context);

        if (!empty($data)) {
            $array['data'] = $data;
        }

        return $array;
    }

    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return array_key_exists('ws_seo_serialization', $context)
            && $data instanceof MetaTagInterface
            && 'json' === $format
        ;
    }

    public function getSupportedTypes(?string $format): array
    {
        return [
            MetaTagInterface::class => true,
        ];
    }
}
