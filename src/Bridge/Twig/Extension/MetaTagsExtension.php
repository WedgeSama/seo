<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use WS\Library\Seo\Bridge\Twig\Extension\Runtime\MetaTagsRuntime;

/**
 * Class MetaTagsExtension
 *
 * @author Benjamin Georgeault
 */
class MetaTagsExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('ws_seo_metatags', [MetaTagsRuntime::class, 'renderMetatags'], [
                'is_safe' => ['html'],
            ]),
        ];
    }
}
