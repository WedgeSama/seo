<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Twig\Extension\Runtime;

use Twig\Extension\RuntimeExtensionInterface;
use WS\Library\Seo\MetaTags\Generator\GeneratorInterface;
use WS\Library\Seo\MetaTags\Model\HaveMetaTagBagInterface;
use WS\Library\Seo\MetaTags\Model\MetaTagBagInterface;

/**
 * Class MetaTagsRuntime
 *
 * @author Benjamin Georgeault
 */
readonly class MetaTagsRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private GeneratorInterface $generator,
    ) {}

    public function renderMetatags(MetaTagBagInterface|HaveMetaTagBagInterface $bag): string
    {
        if ($bag instanceof HaveMetaTagBagInterface) {
            $bag = $bag->getMetaTagBag();
        }

        return $this->generator->generate($bag);
    }
}
