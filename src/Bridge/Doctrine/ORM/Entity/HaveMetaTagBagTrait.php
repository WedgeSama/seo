<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Doctrine\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\Seo\MetaTags\Model\MetaTagBagInterface;

/**
 * Trait HaveMetaTagBagTrait
 *
 * @author Benjamin Georgeault
 */
trait HaveMetaTagBagTrait
{
    #[ORM\Embedded]
    protected ?MetaTagBag $metaTagBag = null;

    public function getMetaTagBag(): ?MetaTagBagInterface
    {
        return $this->metaTagBag ?? $this->metaTagBag = new MetaTagBag();
    }
}
