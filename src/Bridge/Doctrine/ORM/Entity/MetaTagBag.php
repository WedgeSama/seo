<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Doctrine\ORM\Entity;

use Doctrine\ORM\Mapping as ORM;
use WS\Library\Seo\MetaTags\Model\MetaTagBagInterface;
use WS\Library\Seo\MetaTags\Model\MetaTagInterface;

/**
 * Class MetaTagBag
 *
 * @author Benjamin Georgeault
 */
#[ORM\Embeddable]
class MetaTagBag implements MetaTagBagInterface
{
    #[ORM\Column(type: 'json')]
    private array $data = [];

    private array $metaTags = [];

    private bool $_initialized = false;

    /** @var null|callable */
    private $_initializer = null;

    /**
     * @internal
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @internal
     */
    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }

    public function getMetaTags(): ?iterable
    {
        $this->_initialize();

        return empty($this->metaTags) ? null : $this->metaTags;
    }

    public function addMetaTag(MetaTagInterface $metaTag): static
    {
        $this->_initialize();

        if (!in_array($metaTag, $this->metaTags, true)) {
            $this->metaTags[] = $metaTag;
        }

        return $this;
    }

    public function removeMetaTag(MetaTagInterface $metaTag): static
    {
        $this->_initialize();

        if (false !== $key = array_search($metaTag, $this->metaTags, true)) {
            unset($this->metaTags[$key]);
        }

        return $this;
    }

    /**
     * @internal
     */
    public function setInitializer(callable $initializer): static
    {
        $this->_initializer = $initializer;
        return $this;
    }

    private function _initialize(): void
    {
        if ($this->_initialized) {
            return;
        }

        if (null !== $this->_initializer) {
            $this->metaTags = ($this->_initializer)($this->data);
        }

        $this->_initialized = true;
    }
}
