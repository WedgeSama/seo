<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostLoadEventArgs;
use Doctrine\ORM\Events;
use WS\Library\Seo\Bridge\Doctrine\ORM\Entity\MetaTagBag;
use WS\Library\Seo\MetaTags\Model\HaveMetaTagBagInterface;
use WS\Library\Seo\MetaTags\Serializer\SerializerInterface;

/**
 * Class MetaTagBagSubscriber
 *
 * @author Benjamin Georgeault
 */
readonly class MetaTagBagSubscriber implements EventSubscriber
{
    public function __construct(
        private SerializerInterface $serializer,
    ) {}

    public function getSubscribedEvents(): array
    {
        return [
            Events::postLoad,
            Events::onFlush,
        ];
    }

    public function postLoad(PostLoadEventArgs $event): void
    {
        $entity = $event->getObject();
        if (!$entity instanceof MetaTagBag) {
            return;
        }

        $entity->setInitializer([$this->serializer, 'deserialize']);
    }

    public function onFlush(OnFlushEventArgs $event): void
    {
        $om = $event->getObjectManager();
        $uow = $om->getUnitOfWork();

        $entities = array_filter(
            array_merge($uow->getScheduledEntityInsertions(), $uow->getScheduledEntityUpdates()),
            function (object $entity) {
                return $entity instanceof HaveMetaTagBagInterface && $entity->getMetaTagBag() instanceof MetaTagBag;
            },
        );

        foreach ($entities as $entity) {
            $metaTagBag = $entity->getMetaTagBag();
            $metaTagBag->setData($this->serializer->serialize($metaTagBag->getMetaTags()));
            $uow->computeChangeSet($classMetadata = $om->getClassMetadata(get_class($entity)), $entity);
            $uow->recomputeSingleEntityChangeSet($classMetadata, $entity);
        }
    }
}
