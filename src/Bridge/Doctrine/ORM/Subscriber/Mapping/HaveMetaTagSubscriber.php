<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use WS\Library\Seo\Bridge\Doctrine\ORM\Entity\MetaTagBag;
use WS\Library\Seo\MetaTags\Model\HaveMetaTagBagInterface;

/**
 * Class HaveMetaTagSubscriber
 *
 * @author Benjamin Georgeault
 */
class HaveMetaTagSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $metadata = $eventArgs->getClassMetadata();
        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();

        if ($reflectionClass->implementsInterface(HaveMetaTagBagInterface::class)) {
            $this->mappingHaveMetaTag($metadata);
        }
    }

    private function mappingHaveMetaTag(ClassMetadata $metadata): void
    {
        if (!$metadata->hasField('metaTagBag')) {
            $metadata->mapEmbedded([
                'fieldName' => 'metaTagBag',
                'class' => MetaTagBag::class,
            ]);
        }
    }
}
