<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model\UserAgents;

/**
 * Class BingEnum
 *
 * @author Benjamin Georgeault
 * @see https://www.bing.com/webmasters/help/which-crawlers-does-bing-use-8c184ec0
 */
enum BingEnum: string
{
    case BINGBOT = 'Bingbot';
    case AD_IDX_BOT = 'AdIdxBot';
    case BING_PREVIEW = 'BingPreview';
    case MICROSOFT_PREVIEW = 'MicrosoftPreview';
}
