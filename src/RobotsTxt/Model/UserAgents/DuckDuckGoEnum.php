<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model\UserAgents;

/**
 * Class DuckDuckGoEnum
 *
 * @author Benjamin Georgeault
 * @see https://duckduckgo.com/duckduckgo-help-pages/results/duckduckbot/
 * @see https://duckduckgo.com/duckduckgo-help-pages/results/duckassistbot/
 */
enum DuckDuckGoEnum: string
{
    case DUCK_DUCK_BOT = 'DuckDuckBot';
    case DUCK_ASSIST_BOT = 'DuckAssistBot';
}
