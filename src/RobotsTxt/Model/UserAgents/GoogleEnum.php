<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model\UserAgents;

/**
 * Class GoogleEnum
 *
 * @author Benjamin Georgeault
 * @see https://developers.google.com/search/docs/crawling-indexing/overview-google-crawlers
 */
enum GoogleEnum: string
{
    // Commons crawler
    case GOOGLEBOT = 'Googlebot';
    case GOOGLE_IMAGE = 'Googlebot-Image';
    case GOOGLE_NEWS = 'Googlebot-News';
    case GOOGLE_VIDEO = 'Googlebot-Video';
    case GOOGLE_STORE = 'Storebot-Google';
    case GOOGLE_INSPECTION_TOOLS = 'Googlebot-InspectionTool';
    case GOOGLE_OTHER = 'GoogleOther';
    case GOOGLE_OTHER_IMAGE = 'GoogleOther-Image';
    case GOOGLE_OTHER_VIDEO = 'GoogleOther-Video';
    case GOOGLE_EXTENDED = 'Google-Extended';

    // Special-case crawlers
    case APIS_GOOGLE = 'APIs-Google';
    case ADS_BOT_MOBILE = 'AdsBot-Google-Mobile';
    case ADS_BOT = 'AdsBot-Google';
    case ADSENSE = 'Mediapartners-Google';
    case GOOGLE_SAFETY = 'Google-Safety';

    // User-triggered fetchers
    case FEEDFETCHER = 'FeedFetcher-Google';
    case GOOGLE_PUBLISHER_CENTER = 'GoogleProducer';
    case GOOGLE_READ_ALOUD = 'Google-Read-Aloud';
    case GOOGLE_SITE_VERIFICATION = 'Google-Site-Verification';
}
