<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model;

use WS\Library\Seo\RobotsTxt\Model\Directives\DirectiveInterface;

/**
 * Interface UserAgentInterface
 *
 * @author Benjamin Georgeault
 */
interface UserAgentInterface
{
    public function getName(): string;

    /**
     * @return iterable<DirectiveInterface>
     */
    public function getDirectives(): iterable;

    public function getComment(): ?string;
}
