<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model;

/**
 * Class Sitemap
 *
 * @author Benjamin Georgeault
 */
final class Sitemap implements SitemapInterface
{
    private string $location;

    private ?string $comment = null;

    public static function create(string $location, ?string $comment = null): Sitemap
    {
        return (new self())
            ->setLocation($location)
            ->setComment($comment)
        ;
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function setLocation(string $location): Sitemap
    {
        $this->location = $location;
        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): Sitemap
    {
        $this->comment = $comment;
        return $this;
    }
}
