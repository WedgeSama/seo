<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model;

/**
 * Interface RobotsTxtInterface
 *
 * @author Benjamin Georgeault
 */
interface RobotsTxtInterface
{
    /**
     * @return iterable<SitemapInterface>
     */
    public function getSitemaps(): iterable;

    /**
     * @return iterable<UserAgentInterface>
     */
    public function getUserAgents(): iterable;

    public function getComment(): ?string;
}
