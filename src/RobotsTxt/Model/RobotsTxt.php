<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model;

/**
 * Class RobotsTxt
 *
 * @author Benjamin Georgeault
 */
final class RobotsTxt implements RobotsTxtInterface
{
    /** @var iterable<SitemapInterface> */
    private iterable $sitemaps = [];

    /** @var iterable<UserAgentInterface> */
    private iterable $userAgents = [];

    private ?string $comment = null;

    public static function create(
        iterable $userAgents = [],
        iterable $sitemaps = [],
        ?string $comment = null,
    ): RobotsTxt {
        return (new self())
            ->setSitemaps($sitemaps)
            ->setUserAgents($userAgents)
            ->setComment($comment)
        ;
    }

    public function getSitemaps(): iterable
    {
        return $this->sitemaps;
    }

    public function setSitemaps(iterable $sitemaps): RobotsTxt
    {
        $this->sitemaps = $sitemaps;
        return $this;
    }

    public function getUserAgents(): iterable
    {
        return $this->userAgents;
    }

    public function setUserAgents(iterable $userAgents): RobotsTxt
    {
        $this->userAgents = $userAgents;
        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): RobotsTxt
    {
        $this->comment = $comment;
        return $this;
    }
}
