<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model;

use WS\Library\Seo\RobotsTxt\Model\Directives\DirectiveInterface;

/**
 * Class UserAgent
 *
 * @author Benjamin Georgeault
 */
final class UserAgent implements UserAgentInterface
{
    private string $name;

    /** @var iterable<DirectiveInterface> */
    private iterable $directives = [];

    private ?string $comment = null;

    public static function create(string $name, iterable $directives, ?string $comment = null): UserAgent
    {
        return (new self())
            ->setName($name)
            ->setDirectives($directives)
            ->setComment($comment)
        ;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): UserAgent
    {
        $this->name = $name;
        return $this;
    }

    public function getDirectives(): iterable
    {
        return $this->directives;
    }

    public function setDirectives(iterable $directives): UserAgent
    {
        $this->directives = $directives;
        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): UserAgent
    {
        $this->comment = $comment;
        return $this;
    }
}
