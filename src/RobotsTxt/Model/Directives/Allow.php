<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model\Directives;

/**
 * Class Allow
 *
 * @author Benjamin Georgeault
 */
final class Allow implements DirectiveInterface
{
    private ?string $path = null;

    private ?string $comment = null;

    public static function create(?string $path = null, ?string $comment = null): Allow
    {
        return (new self())
            ->setPath($path)
            ->setComment($comment)
        ;
    }

    public function getName(): string
    {
        return 'Allow';
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): Allow
    {
        $this->path = $path;
        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): Allow
    {
        $this->comment = $comment;
        return $this;
    }
}
