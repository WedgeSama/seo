<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Model\Directives;

/**
 * Interface DirectiveInterface
 *
 * @author Benjamin Georgeault
 */
interface DirectiveInterface
{
    public function getName(): string;

    public function getPath(): ?string;

    public function getComment(): ?string;
}
