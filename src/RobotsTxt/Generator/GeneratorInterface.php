<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Generator;

use WS\Library\Seo\RobotsTxt\Model\RobotsTxtInterface;

/**
 * Interface GeneratorInterface
 *
 * @author Benjamin Georgeault
 */
interface GeneratorInterface
{
    public function generate(RobotsTxtInterface $robotsTxt, ?string $fileName = null): string|bool;
}
