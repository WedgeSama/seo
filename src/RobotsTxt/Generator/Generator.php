<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Generator;

use WS\Library\Seo\RobotsTxt\Model\RobotsTxtInterface;

/**
 * Class Generator
 *
 * @author Benjamin Georgeault
 */
final class Generator implements GeneratorInterface
{
    public function generate(RobotsTxtInterface $robotsTxt, ?string $fileName = null): string|bool
    {
        $robotsTxtOutput = '';

        if (null !== $comment = $robotsTxt->getComment()) {
            $robotsTxtOutput .= $this->processComment($comment);
        }

        foreach ($robotsTxt->getUserAgents() as $userAgent) {
            if (!empty($robotsTxtOutput)) {
                $robotsTxtOutput .= "\n\n";
            }

            if (null !== $comment = $userAgent->getComment()) {
                $robotsTxtOutput .= $this->processComment($comment) . "\n";
            }

            $robotsTxtOutput .= 'User-agent: ' . $userAgent->getName();

            foreach ($userAgent->getDirectives() as $directive) {
                $robotsTxtOutput .= "\n";

                if (null !== $comment = $directive->getComment()) {
                    $robotsTxtOutput .= $this->processComment($comment) . "\n";
                }

                $robotsTxtOutput .= $directive->getName();

                if (null !== $path = $directive->getPath()) {
                    $robotsTxtOutput .= ': ' . $path;
                }
            }
        }

        foreach ($robotsTxt->getSitemaps() as $sitemap) {
            if (!empty($robotsTxtOutput)) {
                $robotsTxtOutput .= "\n\n";
            }

            if (null !== $comment = $sitemap->getComment()) {
                $robotsTxtOutput .= $this->processComment($comment) . "\n";
            }

            $robotsTxtOutput .= 'Sitemap: ' . $sitemap->getLocation();
        }

        if (!empty($robotsTxtOutput)) {
            $robotsTxtOutput .= "\n";
        }

        if (null !== $fileName) {
            return false !== file_put_contents($fileName, $robotsTxtOutput);
        }

        return $robotsTxtOutput;
    }

    private function processComment(string $comment): string
    {
        $lines = preg_split('/\r\n|\r|\n/', $comment);
        $lines = array_map(function ($line) {
            return "# $line";
        }, $lines);

        return implode(PHP_EOL, $lines);
    }
}
