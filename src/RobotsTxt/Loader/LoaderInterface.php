<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Loader;

use WS\Library\Seo\RobotsTxt\Model\RobotsTxtInterface;

/**
 * Interface LoaderInterface
 *
 * @author Benjamin Georgeault
 */
interface LoaderInterface
{
    public function load(string $host): RobotsTxtInterface;

    public function supports(string $host): bool;
}
