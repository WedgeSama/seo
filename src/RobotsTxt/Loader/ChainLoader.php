<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Library\Seo\RobotsTxt\Loader;

use WS\Library\Seo\RobotsTxt\Exception\LoaderException;
use WS\Library\Seo\RobotsTxt\Model\RobotsTxtInterface;

/**
 * Class ChainLoader
 *
 * @author Benjamin Georgeault
 */
readonly final class ChainLoader implements LoaderInterface
{
    public function __construct(
        /** @var iterable<LoaderInterface> */
        private iterable $loaders,
    ) {}

    public function load(string $host): RobotsTxtInterface
    {
        foreach ($this->getLoaders() as $loader) {
            if ($loader->supports($host)) {
                return $loader->load($host);
            }
        }

        throw new LoaderException(sprintf('No loader found for host "%s".', $host));
    }

    public function supports(string $host): bool
    {
        foreach ($this->getLoaders() as $loader) {
            if ($loader->supports($host)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return iterable<LoaderInterface>
     */
    private function getLoaders(): iterable
    {
        foreach ($this->loaders as $loader) {
            if (!$loader instanceof LoaderInterface) {
                throw new \InvalidArgumentException(sprintf('Loader must implement %s', LoaderInterface::class));
            }

            yield $loader;
        }
    }
}
