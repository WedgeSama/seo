Seo library
===========

A library to enhance SEO of an PHP project.

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
