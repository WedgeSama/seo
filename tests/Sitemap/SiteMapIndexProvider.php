<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\Seo\Sitemap;

use WS\Library\Seo\Sitemap\Model\SiteMap;
use WS\Library\Seo\Sitemap\Model\SiteMapIndex;
use WS\Library\Seo\Sitemap\Model\SiteMapIndexInterface;

/**
 * Class SiteMapIndexProvider
 *
 * @author Benjamin Georgeault
 */
class SiteMapIndexProvider
{
    public static function getBasic(): SiteMapIndexInterface
    {
        return SiteMapIndex::create([
            SiteMap::create('https://foobar.com/sitemap-page.xml'),
            SiteMap::create('https://foobar.com/sitemap-news.xml', new \DateTimeImmutable('2021-01-01T00:00:00+00:00')),
            SiteMap::create('https://foobar.com/sitemap-videos.xml'),
        ]);
    }

    public static function getBasicExpected(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><sitemap><loc>https://foobar.com/sitemap-page.xml</loc></sitemap><sitemap><loc>https://foobar.com/sitemap-news.xml</loc><lastmod>2021-01-01T00:00:00+00:00</lastmod></sitemap><sitemap><loc>https://foobar.com/sitemap-videos.xml</loc></sitemap></sitemapindex>

XML;
    }
}
