<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\Seo\Sitemap\Generator;

use PHPUnit\Framework\TestCase;
use WS\Library\Seo\Sitemap\Exception\UniqueElementException;
use WS\Library\Seo\Sitemap\Generator\Generator;
use WS\Tests\Library\Seo\Sitemap\SiteMapIndexProvider;
use WS\Tests\Library\Seo\Sitemap\UrlSetProvider;

/**
 * Class GeneratorTest
 *
 * @author Benjamin Georgeault
 */
class GeneratorTest extends TestCase
{
    public function testSiteMapIndex(): void
    {
        $siteMapIndex = SiteMapIndexProvider::getBasic();
        $generator = new Generator();

        $xml = $generator->generate($siteMapIndex);

        $this->assertEquals(SiteMapIndexProvider::getBasicExpected(), $xml);
    }

    public function testUrlSet(): void
    {
        $urlSet = UrlSetProvider::getBasic();
        $generator = new Generator();

        $xml = $generator->generate($urlSet);

        $this->assertEquals(UrlSetProvider::getBasicExpected(), $xml);
    }

    public function testUrlSetWithAlternateLanguage(): void
    {
        $urlSet = UrlSetProvider::getWithAlternateLanguage();
        $generator = new Generator();

        $xml = $generator->generate($urlSet);

        $this->assertEquals(UrlSetProvider::getWithAlternateLanguageExpected(), $xml);
    }

    public function testUrlSetWithNews(): void
    {
        $generator = new Generator();

        $xml = $generator->generate(UrlSetProvider::getWithNews());
        $this->assertEquals(UrlSetProvider::getWithNewsExpected(), $xml);

        $this->expectException(UniqueElementException::class);
        $generator->generate(UrlSetProvider::getWithNewsAndError());
    }

    public function testUrlSetWithImage(): void
    {
        $generator = new Generator();

        $xml = $generator->generate(UrlSetProvider::getWithImage());
        $this->assertEquals(UrlSetProvider::getWithImageExpected(), $xml);
    }

    public function testUrlSetWithVideo(): void
    {
        $generator = new Generator();

        $xml = $generator->generate(UrlSetProvider::getBasicWithVideo());
        $this->assertEquals(UrlSetProvider::getBasicWithVideoExpected(), $xml);

        $xml = $generator->generate(UrlSetProvider::getWithVideoFullInfo());
        $this->assertEquals(UrlSetProvider::getWithVideoFullInfoExpected(), $xml);
    }
}
