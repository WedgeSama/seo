<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\Seo\Sitemap;

use WS\Library\Seo\Sitemap\Model\ChangeFreqEnum;
use WS\Library\Seo\Sitemap\Model\Url;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\AlternateLanguage;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\Image;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\News;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\Video;
use WS\Library\Seo\Sitemap\Model\UrlAdditionalInfo\VideoPlatformEnum;
use WS\Library\Seo\Sitemap\Model\UrlSet;
use WS\Library\Seo\Sitemap\Model\UrlSetInterface;

/**
 * Class UrlSetProvider
 *
 * @author Benjamin Georgeault
 */
class UrlSetProvider
{
    public static function getBasic(): UrlSetInterface
    {
        return UrlSet::create([
            Url::create(
                'https://foobar.com/',
                new \DateTimeImmutable('2021-01-01T00:00:00+00:00'),
                ChangeFreqEnum::DAILY,
                1.0,
            ),
            Url::create(
                'https://foobar.com/info',
                new \DateTimeImmutable('2021-01-05T00:00:00+00:00'),
                priority: 0.8,
            ),
            Url::create(
                'https://foobar.com/contact',
                new \DateTimeImmutable('2021-01-15T00:00:00+00:00'),
                ChangeFreqEnum::MONTHLY,
            ),
            Url::create(
                'https://foobar.com/our-team',
                new \DateTimeImmutable('2021-05-05T00:00:00+00:00'),
                ChangeFreqEnum::YEARLY,
            ),
        ]);
    }

    public static function getBasicExpected(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"><url><loc>https://foobar.com/</loc><lastmod>2021-01-01T00:00:00+00:00</lastmod><changefreq>daily</changefreq><priority>1.0</priority></url><url><loc>https://foobar.com/info</loc><lastmod>2021-01-05T00:00:00+00:00</lastmod><priority>0.8</priority></url><url><loc>https://foobar.com/contact</loc><lastmod>2021-01-15T00:00:00+00:00</lastmod><changefreq>monthly</changefreq></url><url><loc>https://foobar.com/our-team</loc><lastmod>2021-05-05T00:00:00+00:00</lastmod><changefreq>yearly</changefreq></url></urlset>

XML;
    }

    public static function getWithAlternateLanguage(): UrlSetInterface
    {
        return UrlSet::create([
            Url::create(
                'https://foobar.com/fr/',
                additionalInfo: [
                    AlternateLanguage::create(
                        'fr',
                        'https://foobar.com/fr/',
                    ),
                    AlternateLanguage::create(
                        'en',
                        'https://foobar.com/en/',
                    ),
                ],
            ),
            Url::create(
                'https://foobar.com/fr/info',
                additionalInfo: [
                    AlternateLanguage::create(
                        'fr',
                        'https://foobar.com/fr/info/',
                    ),
                    AlternateLanguage::create(
                        'en',
                        'https://foobar.com/en/info/',
                    ),
                ],
            ),
        ]);
    }

    public static function getWithAlternateLanguageExpected(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"><url><loc>https://foobar.com/fr/</loc><xhtml:link rel="alternate" hreflang="fr" href="https://foobar.com/fr/"/><xhtml:link rel="alternate" hreflang="en" href="https://foobar.com/en/"/></url><url><loc>https://foobar.com/fr/info</loc><xhtml:link rel="alternate" hreflang="fr" href="https://foobar.com/fr/info/"/><xhtml:link rel="alternate" hreflang="en" href="https://foobar.com/en/info/"/></url></urlset>

XML;
    }

    public static function getWithNews(): UrlSetInterface
    {
        return UrlSet::create([
            Url::create(
                'https://foobar.com/fr/',
            ),
            Url::create(
                'https://foobar.com/fr/blog/news-1/',
                additionalInfo: [
                    News::create(
                        'Lorem ipsum info',
                        'Foo magazine',
                        'fr',
                        new \DateTimeImmutable('2021-01-01T00:00:00+00:00'),
                    ),
                ],
            ),
        ]);
    }

    public static function getWithNewsExpected(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"><url><loc>https://foobar.com/fr/</loc></url><url><loc>https://foobar.com/fr/blog/news-1/</loc><news:news><news:publication><news:name>Foo magazine</news:name><news:language>fr</news:language></news:publication><news:publication_date>2021-01-01T00:00:00+00:00</news:publication_date><news:title>Lorem ipsum info</news:title></news:news></url></urlset>

XML;
    }

    public static function getWithNewsAndError(): UrlSetInterface
    {
        return UrlSet::create([
            Url::create(
                'https://foobar.com/fr/blog/news-1/',
                additionalInfo: [
                    News::create(
                        'Lorem ipsum info',
                        'Foo magazine',
                        'fr',
                        new \DateTimeImmutable('2021-01-01T00:00:00+00:00'),
                    ),
                    new News(),
                ],
            ),
        ]);
    }

    public static function getWithImage(): UrlSetInterface
    {
        return UrlSet::create([
            Url::create(
                'https://foobar.com/fr/blog/news-1/',
                additionalInfo: [
                    Image::create(
                        'https://foobar.com/images/news-1.jpg',
                    ),
                    Image::create(
                        'https://foobar.com/images/more-news-1.jpg',
                    ),
                ],
            ),
        ]);
    }

    public static function getWithImageExpected(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"><url><loc>https://foobar.com/fr/blog/news-1/</loc><image:image><image:loc>https://foobar.com/images/news-1.jpg</image:loc></image:image><image:image><image:loc>https://foobar.com/images/more-news-1.jpg</image:loc></image:image></url></urlset>

XML;
    }

    public static function getBasicWithVideo(): UrlSetInterface
    {
        return UrlSet::create([
            Url::create(
                'https://foobar.com/fr/blog/news-1/',
                additionalInfo: [
                    Video::create(
                        'https://foobar.com/thumbnails/news-1.jpg',
                        'Lorem ipsum video',
                        'Lorem ipsum description',
                        'https://foobar.com/videos/news-1.mp4',
                    ),
                ],
            ),
        ]);
    }

    public static function getBasicWithVideoExpected(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"><url><loc>https://foobar.com/fr/blog/news-1/</loc><video:video><video:thumbnail_loc>https://foobar.com/thumbnails/news-1.jpg</video:thumbnail_loc><video:title>Lorem ipsum video</video:title><video:description>Lorem ipsum description</video:description><video:content_loc>https://foobar.com/videos/news-1.mp4</video:content_loc></video:video></url></urlset>

XML;
    }

    public static function getWithVideoFullInfo(): UrlSetInterface
    {
        return UrlSet::create([
            Url::create(
                'https://foobar.com/fr/blog/news-1/',
                additionalInfo: [
                    Video::create(
                        'https://foobar.com/thumbnails/news-1.jpg',
                        'Lorem ipsum video',
                        'Lorem ipsum description',
                        'https://foobar.com/videos/news-1.mp4',
                        duration: 60,
                        expirationDate: new \DateTimeImmutable('2021-12-01T00:00:00+00:00'),
                        rating: 4.5,
                        viewCount: 5000,
                        publicationDate: new \DateTimeImmutable('2021-01-01T00:00:00+00:00'),
                        familyFriendly: true,
                        restriction: ['fr', 'en'],
                        platform: [VideoPlatformEnum::TV, VideoPlatformEnum::WEB],
                        requiresSubscription: false,
                        uploader: 'Foo bar',
                        uploaderInfo: 'https://foobar.com/fr/blog/news-1/uploader',
                        live: false,
                        tag: 'Lorem ipsum',
                    ),
                ],
            ),
        ]);
    }

    public static function getWithVideoFullInfoExpected(): string
    {
        return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"><url><loc>https://foobar.com/fr/blog/news-1/</loc><video:video><video:thumbnail_loc>https://foobar.com/thumbnails/news-1.jpg</video:thumbnail_loc><video:title>Lorem ipsum video</video:title><video:description>Lorem ipsum description</video:description><video:content_loc>https://foobar.com/videos/news-1.mp4</video:content_loc><video:duration>60</video:duration><video:expiration_date>2021-12-01T00:00:00+00:00</video:expiration_date><video:rating>4.5</video:rating><video:view_count>5000</video:view_count><video:publication_date>2021-01-01T00:00:00+00:00</video:publication_date><video:family_friendly>yes</video:family_friendly><video:restriction relationship="allow">fr en</video:restriction><video:platform relationship="allow">tv web</video:platform><video:requires_subscription>yes</video:requires_subscription><video:uploader info="https://foobar.com/fr/blog/news-1/uploader">Foo bar</video:uploader></video:video></url></urlset>

XML;
    }
}
