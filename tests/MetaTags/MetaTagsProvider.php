<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\Seo\MetaTags;

use WS\Library\Seo\MetaTags\Model\Custom;
use WS\Library\Seo\MetaTags\Model\Description;
use WS\Library\Seo\MetaTags\Model\MetaTagBagInterface;
use WS\Library\Seo\MetaTags\Model\Rating;
use WS\Library\Seo\MetaTags\Model\RatingEnum;
use WS\Library\Seo\MetaTags\Model\RobotRules\All;
use WS\Library\Seo\MetaTags\Model\RobotRules\IndexIfEmbedded;
use WS\Library\Seo\MetaTags\Model\RobotRules\MaxImagePreview;
use WS\Library\Seo\MetaTags\Model\RobotRules\MaxSnippet;
use WS\Library\Seo\MetaTags\Model\RobotRules\MaxVideoPreview;
use WS\Library\Seo\MetaTags\Model\RobotRules\NoArchive;
use WS\Library\Seo\MetaTags\Model\RobotRules\NoFollow;
use WS\Library\Seo\MetaTags\Model\RobotRules\NoImageIndex;
use WS\Library\Seo\MetaTags\Model\RobotRules\NoIndex;
use WS\Library\Seo\MetaTags\Model\RobotRules\None;
use WS\Library\Seo\MetaTags\Model\RobotRules\NoSiteLinksSearchBox;
use WS\Library\Seo\MetaTags\Model\RobotRules\NoSnippet;
use WS\Library\Seo\MetaTags\Model\RobotRules\NoTranslate;
use WS\Library\Seo\MetaTags\Model\RobotRules\UnavailableAfter;
use WS\Library\Seo\MetaTags\Model\Robots;

/**
 * Class MetaTagsProvider
 *
 * @author Benjamin Georgeault
 */
class MetaTagsProvider
{
    public static function getBasic(): MetaTagBagInterface
    {
        return new class implements MetaTagBagInterface {
            public function getMetaTags(): ?iterable
            {
                return [
                    Description::create('This is a description'),
                    Rating::create(RatingEnum::SAFE_FOR_KIDS),
                    Robots::create([
                        NoArchive::create(),
                    ]),
                ];
            }
        };
    }

    public static function getBasicExpected(): string
    {
        return <<<EXPECTED
<meta name="description" content="This is a description">
<meta name="rating" content="safe for kids">
<meta name="robots" content="noarchive">

EXPECTED;
    }

    public static function getRobots(): MetaTagBagInterface
    {
        return new class implements MetaTagBagInterface {
            public function getMetaTags(): ?iterable
            {
                return [
                    Robots::create([
                        All::create(),
                        IndexIfEmbedded::create(),
                        MaxImagePreview::create(),
                        MaxSnippet::create(100),
                        MaxVideoPreview::create(10),
                        NoArchive::create(),
                        NoFollow::create(),
                        NoImageIndex::create(),
                        NoIndex::create(),
                        None::create(),
                        NoSiteLinksSearchBox::create(),
                        NoSnippet::create(),
                        NoTranslate::create(),
                        UnavailableAfter::create(new \DateTime('2021-01-01T00:00:00+00:00')),
                    ]),
                ];
            }
        };
    }

    public static function getRobotsExpected(): string
    {
        return <<<EXPECTED
<meta name="robots" content="all, indexifembedded, max-image-preview:standard, max-snippet:100, max-video-preview:10, noarchive, nofollow, noimageindex, noindex, none, nositelinkssearchbox, nosnippet, notranslate, unavailable_after:2021-01-01T00:00:00+00:00">

EXPECTED;
    }
}
