<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\Seo\MetaTags\Generator;

use PHPUnit\Framework\TestCase;
use WS\Library\Seo\MetaTags\Generator\Generator;
use WS\Tests\Library\Seo\MetaTags\MetaTagsProvider;

/**
 * Class GeneratorTest
 *
 * @author Benjamin Georgeault
 */
class GeneratorTest extends TestCase
{
    public function testBasic(): void
    {
        $generator = new Generator();

        $html = $generator->generate(MetaTagsProvider::getBasic());

        $this->assertEquals(MetaTagsProvider::getBasicExpected(), $html);
    }

    public function testAdvance(): void
    {
        $generator = new Generator();

        $html = $generator->generate(MetaTagsProvider::getRobots());

        $this->assertEquals(MetaTagsProvider::getRobotsExpected(), $html);
    }
}
