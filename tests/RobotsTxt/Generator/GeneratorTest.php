<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\Seo\RobotsTxt\Generator;

use PHPUnit\Framework\TestCase;
use WS\Library\Seo\RobotsTxt\Generator\Generator;
use WS\Tests\Library\Seo\RobotsTxt\RobotsTxtProvider;

/**
 * Class GeneratorTest
 *
 * @author Benjamin Georgeault
 */
class GeneratorTest extends TestCase
{
    public function testBasicGeneration(): void
    {
        $generator = new Generator();

        $output = $generator->generate(RobotsTxtProvider::getBasic());
        $this->assertEquals(RobotsTxtProvider::getBasicExpected(), $output);
    }

    public function testAdvanceGeneration(): void
    {
        $generator = new Generator();

        $output = $generator->generate(RobotsTxtProvider::getAdvance());
        $this->assertEquals(RobotsTxtProvider::getAdvanceExpected(), $output);
    }
}
