<?php
/*
 * This file is part of the seo package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WS\Tests\Library\Seo\RobotsTxt;

use WS\Library\Seo\RobotsTxt\Model\Directives\Allow;
use WS\Library\Seo\RobotsTxt\Model\Directives\Disallow;
use WS\Library\Seo\RobotsTxt\Model\RobotsTxt;
use WS\Library\Seo\RobotsTxt\Model\RobotsTxtInterface;
use WS\Library\Seo\RobotsTxt\Model\Sitemap;
use WS\Library\Seo\RobotsTxt\Model\UserAgent;
use WS\Library\Seo\RobotsTxt\Model\UserAgents\GoogleEnum;

/**
 * Class RobotsTxtProvider
 *
 * @author Benjamin Georgeault
 */
class RobotsTxtProvider
{
    public static function getBasic(): RobotsTxtInterface
    {
        return RobotsTxt::create([
            UserAgent::create('*', [
                Disallow::create('/admin/'),
            ]),
        ], [
            Sitemap::create('sitemap.xml'),
        ]);
    }

    public static function getBasicExpected(): string
    {
        return <<<TXT
User-agent: *
Disallow: /admin/

Sitemap: sitemap.xml

TXT;
    }

    public static function getAdvance(): RobotsTxtInterface
    {
        return RobotsTxt::create([
            UserAgent::create('*', [
                Disallow::create('/admin/'),
                Disallow::create('/ajax/'),
                Disallow::create('/api/'),
                Disallow::create('/beta/'),
            ], 'Default directives for all robots.'),
            UserAgent::create(GoogleEnum::GOOGLE_NEWS->value, [
                Allow::create('/news/'),
            ]),
            UserAgent::create(GoogleEnum::GOOGLE_OTHER->value, [
                Disallow::create('/'),
                Allow::create('/other/'),
            ]),
        ], [
            Sitemap::create('https://foobar.com/sitemap_news.xml'),
            Sitemap::create('https://foobar.com/sitemap_pages.xml'),
        ]);
    }

    public static function getAdvanceExpected(): string
    {
        return <<<TXT
# Default directives for all robots.
User-agent: *
Disallow: /admin/
Disallow: /ajax/
Disallow: /api/
Disallow: /beta/

User-agent: Googlebot-News
Allow: /news/

User-agent: GoogleOther
Disallow: /
Allow: /other/

Sitemap: https://foobar.com/sitemap_news.xml

Sitemap: https://foobar.com/sitemap_pages.xml

TXT;
    }
}
